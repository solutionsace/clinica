﻿using BusinessLayer.DAO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BO
{
    public class AtendenteBO
    {
        private SqlServerDao dao;
        public AtendenteBO(SqlServerDao dao)
        {
            this.dao = dao;
        }

        public void Inserir(Atendente atendente)
        {
            if (string.IsNullOrEmpty(atendente.Nome) || string.IsNullOrEmpty(atendente.CPF) || atendente.Codigo <= 0  || atendente.DataNascimento == null)
            {
                //throw new atendente invalido exception
            }else if (ListarTodos().FirstOrDefault(a => a.Codigo == atendente.Codigo || a.CPF == atendente.CPF) != null)
            {
                //throw new atendente repetido
            }else
            {
                dao.Inserir(atendente);
            }

            
        }

        public List<Atendente> ListarTodos()
        {
            return dao.ListarTodos<Atendente>();
        }

        public void Atualizar()
        {
            dao.Atualizar<Atendente>();
        }
    }
}
