﻿using BusinessLayer.DAO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Enum;

namespace BusinessLayer.BO
{
    public class ConsultaBO
    {
        private SqlServerDao dao;
        public ConsultaBO(SqlServerDao dao)
        {
            this.dao = dao;
        }

        public void Inserir(Consulta consulta)
        {
            List<Consulta> consultas = ListarConsultas();
            if (consultas.FirstOrDefault(c => c.DataConsulta == consulta.DataConsulta && ((c.Doutor == consulta.Doutor || c.Consultorio == consulta.Consultorio) && c.Id != consulta.Id)) != null)
            {
                throw new Exception("Doutor ou Consultorio já ocupados nesta data.");
            }
            Pagamento pagamento = new Pagamento();
            pagamento.StatusPagamentoEnum = StatusPagamentoEnum.PENDENTE;
            pagamento.TipoPagamentoEnum = TipoPagamentoEnum.ADEFINIR;
            pagamento.Vencimento = DateTime.Now.AddDays(3);
            pagamento.Valor = 0;
            consulta.Pagamento = pagamento;
            dao.Inserir<Consulta>(consulta);
        }

        internal Consulta BuscarPorId(int id)
        {
            return dao.Set<Consulta>().Include("Paciente").Include("Doutor").Include("Consultorio").Where(c => c.Id == id).FirstOrDefault();
        }

        public List<Consulta> ListarConsultas()
        {
            return dao.Set<Consulta>().Include("Paciente").Include("Doutor").Include("Consultorio").ToList();
            //return dao.ListarTodos<Consulta>();
        }

        
    }
}
