﻿using BusinessLayer.DAO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BO
{
    public class ConsultorioBO
    {
        private SqlServerDao dao;
        public ConsultorioBO(SqlServerDao dao)
        {
            this.dao = dao;
        }

        public void Inserir(Consultorio consultorio)
        {
            if (string.IsNullOrEmpty(consultorio.Nome))
            {
                //throw new consultorio inválido
            }else if (ListarTodos().FirstOrDefault(c => c.Nome == consultorio.Nome) != null)
            {
                //throw new consultório repetido
            }else
            {
                dao.Inserir(consultorio);
            }
            
        }

        public List<Consultorio> ListarTodos()
        {
            return dao.ListarTodos<Consultorio>();
        }

        public void Atualizar()
        {
            dao.Atualizar<Consultorio>();
        }

        internal Consultorio BuscarPorId(int consultorio)
        {
            return dao.BuscarPorId<Consultorio>(consultorio);
        }
    }
}
