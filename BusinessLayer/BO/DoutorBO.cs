﻿using BusinessLayer.DAO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BO
{
    public class DoutorBO
    {
        private SqlServerDao dao;
        public DoutorBO(SqlServerDao dao)
        {
            this.dao = dao;
        }

        public void Inserir(Doutor doutor)
        {
            var lista = ListarTodos();
            if (string.IsNullOrEmpty(doutor.Nome) || string.IsNullOrEmpty(doutor.CPF) || doutor.CRM == null || doutor.DataNascimento == null)
            {
                //throw new doutor inválido
            }else if (lista.FirstOrDefault(d => d.CPF == doutor.CPF) != null)
            {
                //throw new doutor já cadastrado
            }else if (lista.FirstOrDefault(d => d.CRM == doutor.CRM) != null)
            {
                //throw new crm repetido
            }else
            {
                dao.Inserir(doutor);
            }
            
        }

        internal Doutor BuscarPorId(int id)
        {
            return dao.BuscarPorId<Doutor>(id);
        }

        public List<Doutor> ListarTodos()
        {
            return dao.ListarTodos<Doutor>();
        }

        public void Atualizar()
        {
            dao.Atualizar<Doutor>();
        }

    }
}
