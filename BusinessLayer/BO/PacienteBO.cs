﻿using BusinessLayer.DAO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BO
{
    public class PacienteBO
    {
        private SqlServerDao dao;

        public PacienteBO(SqlServerDao dao)
        {
            this.dao = dao;
        }

        public void InserirPaciente(Paciente paciente)
        {

            if (string.IsNullOrEmpty(paciente.Nome) || string.IsNullOrEmpty(paciente.CPF) || paciente.DataNascimento == null)
            {
                throw new Exception("Paciente inválido");
            }
            else if(Listar().FirstOrDefault(p => p.CPF == paciente.CPF) != null)
            {
                throw new Exception("Paciente já Cadastrado!");
            }else
            {
                dao.Inserir(paciente);
            }

            
        }

        public List<Paciente> Listar()
        {
            return dao.ListarTodos<Paciente>();   
        }

        public void Atualizar()
        {
            dao.Atualizar<Paciente>();
        }

        internal Paciente BuscarPorId(int id)
        {
            return dao.BuscarPorId<Paciente>(id);
        }
    }
}
