﻿using BusinessLayer.DAO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BO
{
    public class PagamentoBO
    {
        private SqlServerDao dao;
        public PagamentoBO(SqlServerDao dao)
        {
            this.dao = dao;
        }

        public void InserirPagamento(Pagamento pagamento)
        {
            if (pagamento.Valor == null || pagamento.Vencimento == null || pagamento.StatusPagamentoEnum == null || pagamento.TipoPagamentoEnum == null)
            {
                //throw new pagamento inválido
            }else if(pagamento.Valor <= 0)
            {
                ///throw new o valor deve ser maior que zero
            }else if (pagamento.Vencimento <= DateTime.Now)
            {
                //throw new DataVencimento Exception
            }else
            {
                dao.Inserir(pagamento);
            }
            
        }

        public List<Pagamento> ListarPagamentos()
        {
            return dao.ListarTodos<Pagamento>();
        }

    }
}
