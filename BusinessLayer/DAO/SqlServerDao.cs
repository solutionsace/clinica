﻿using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DAO
{
    public class SqlServerDao : DataLayer.DAO.SqlServerDao
    {
        DbSet<Atendente> AtendenteDAO { get; set; }
        DbSet<Consulta> ConsultaDAO { get; set; }
        DbSet<Consultorio> ConsultorioDAO { get; set; }
        DbSet<Doutor> DoutorDAO { get; set; }
        DbSet<Paciente> PacienteDAO { get; set; }
        DbSet<Pagamento> PagamentoDAO { get; set; }
    }
}
