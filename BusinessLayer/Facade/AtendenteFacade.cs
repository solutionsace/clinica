﻿using BusinessLayer.BO;
using BusinessLayer.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Facade
{
    public class AtendenteFacade : IDisposable
    {
        private SqlServerDao dao;
        private AtendenteBO bo;
        public AtendenteFacade()
        {
            dao = new SqlServerDao();
            bo = new AtendenteBO(dao);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (dao != null) dao.Dispose();
            }
        }
    }
}
