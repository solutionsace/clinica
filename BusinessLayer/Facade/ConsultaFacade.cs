﻿using BusinessLayer.BO;
using BusinessLayer.DAO;
using Model;
using Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Facade
{
    public class ConsultaFacade : IDisposable
    {
        private SqlServerDao dao;
        private ConsultaBO bo;

        public ConsultaFacade()
        {
            dao = new SqlServerDao();
            bo = new ConsultaBO(dao);
        }

        public void InserirConsulta(Consulta consulta)
        {
            bo.Inserir(consulta);
        }

        public List<Consulta> ListarConsultas()
        {

            return bo.ListarConsultas();
        }

               

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (dao != null) dao.Dispose();
            }
        }

        public Consulta BuscarPorId(int id)
        {
            return bo.BuscarPorId(id);
        }
    }
}
