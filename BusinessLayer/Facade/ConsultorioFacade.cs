﻿using BusinessLayer.BO;
using BusinessLayer.DAO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Facade
{
    public class ConsultorioFacade : IDisposable
    {
        private SqlServerDao dao;
        private ConsultorioBO bo;

        public ConsultorioFacade()
        {
            dao = new SqlServerDao();
            bo = new ConsultorioBO(dao);
        }

        public void InserirConsultorio(string nome)
        {
            Consultorio consultorio = new Consultorio { Nome = nome };
            dao.Inserir(consultorio);
        }

        public List<Consultorio> ListarConsultorios()
        {
            return bo.ListarTodos();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (dao != null) dao.Dispose();
            }
        }

        public Consultorio BuscarPorId(int consultorio)
        {
            return bo.BuscarPorId(consultorio);
        }
    }

}
