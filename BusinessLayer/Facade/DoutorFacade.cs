﻿using BusinessLayer.BO;
using BusinessLayer.DAO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Facade
{
    public class DoutorFacade : IDisposable
    {
        private SqlServerDao dao;
        private DoutorBO bo;

        public DoutorFacade()
        {
            dao = new SqlServerDao();
            bo = new DoutorBO(dao);
        }

        public void InserirDoutor(string nome, string cpf, DateTime dataNascimento, int crm)
        {
            Doutor doutor = new Doutor { Nome = nome, CPF = cpf, DataNascimento = dataNascimento, CRM = crm };

            bo.Inserir(doutor);
        }

        public List<Doutor> ListarDoutores()
        {
            return bo.ListarTodos();

        }

        public Doutor BuscarPorId(int doutor)
        {
            return bo.BuscarPorId(doutor);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (dao != null) dao.Dispose();
            }
        }

        
    }
}
