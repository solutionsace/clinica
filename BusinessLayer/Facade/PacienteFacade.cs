﻿using BusinessLayer.BO;
using BusinessLayer.DAO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Facade
{
    public class PacienteFacade : IDisposable
    {

        private SqlServerDao dao;
        private PacienteBO pacienteBO;

        public PacienteFacade()
        {
            dao = new SqlServerDao();
            pacienteBO = new PacienteBO(dao);
        }

        public void InserirPaciente(string nome, string cpf, DateTime dataNascimento, string email, string senha)
        {
            Paciente paciente = new Paciente { Nome = nome, CPF = cpf, DataNascimento = dataNascimento, Email = email, Senha = senha };
           
            pacienteBO.InserirPaciente(paciente);
        }

        public List<Paciente> ListarPacientes(int id = 0)
        {
            if (id == 0)
            {
                return pacienteBO.Listar();
            }else
            {
                return pacienteBO.Listar().Where(p => p.Id == id).ToList();
            }
            
        }
        public Paciente BuscarPorId(int id)
        {
            return pacienteBO.BuscarPorId(id);
        }




        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (dao != null) dao.Dispose();
            }
        }
    }
}
