﻿using BusinessLayer.BO;
using BusinessLayer.DAO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Facade
{
    public class PagamentoFacade : IDisposable
    {

        private SqlServerDao dao;
        private PagamentoBO bo;

        public PagamentoFacade()
        {
            dao = new SqlServerDao();
            bo = new PagamentoBO(dao);
        }

        public void InserirPagamento()
        {

            bo.InserirPagamento(new Pagamento());
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (dao != null) dao.Dispose();
            }
        }
    }
}
