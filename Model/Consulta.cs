﻿using DataLayer.Tabelas;
using Model.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [Table("Consulta")]
    public class Consulta : Base
    {
        [Required]
        public TipoConsultaEnum TipoConsultaEnum { get; set; }
        [Required]
        public StatusConsultaEnum StatusConsultaEnum { get; set; }
        [Required]
        public virtual Doutor Doutor { get; set; }
        [Required]
        public virtual Paciente Paciente { get; set; }
        [Required]
        public virtual Consultorio Consultorio { get; set; }
        [Required]
        public virtual Pagamento Pagamento { get; set; }
        [Required]
        public DateTime DataConsulta { get; set; }
    }
}
