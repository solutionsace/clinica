﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [Table("Doutor")]
    public class Doutor : Pessoa
    {
        [Required]
        public int? CRM { get; set; }
    }
}
