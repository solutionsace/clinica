﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Enum
{
    public enum StatusConsultaEnum
    {
        CONFIRMADA = 1, PENDENTE = 2, CANCELADA = 3
    }
}
