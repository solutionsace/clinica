﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Enum
{
    public enum TipoConsultaEnum
    {
        CONSULTA = 1, CIRURGIA = 2, RETORNO = 3
    }

    public static class TipoConsultaEnumMethods
    {
        public static TipoConsultaEnum GetTipo(string tipo)
        {
            switch (tipo)
            {
                case "CONSULTA":
                    return TipoConsultaEnum.CONSULTA;
                case "CIRURGIA":
                    return TipoConsultaEnum.CIRURGIA;
                case "RETORNO":
                    return TipoConsultaEnum.RETORNO;
                default:
                    return TipoConsultaEnum.CONSULTA;
            }
        }
    }

}
