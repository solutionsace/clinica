﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Enum
{
    public enum TipoPagamentoEnum
    {
        CARTAO = 1, DINHEIRO = 2, CONVENIO = 3, CHEQUE = 4, ADEFINIR = 5
    }
}
