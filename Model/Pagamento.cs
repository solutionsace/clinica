﻿using DataLayer.Tabelas;
using Model.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [Table("Pagamento")]
    public class Pagamento : Base
    {
        [Required]
        public StatusPagamentoEnum? StatusPagamentoEnum { get; set; }
        [Required]
        public TipoPagamentoEnum? TipoPagamentoEnum { get; set; }
        [Required]
        public double? Valor { get; set; }
        [Required]
        public DateTime Vencimento { get; set; }
    }
}
