﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLayer.Facade;
using Model;

namespace Testes
{
    [TestClass]
    public class Testes
    {

        /*
         Tem bastante coisa feita já.. todas as classes facade estão criadas, falta adicionar alguns metodos em algumas delas..
         Falta criar o BO de consultas, todos os outros já estão criados..
         falta também criar uma classe pra controle de excpetions.. mas deixa que mais pra frente eu vou fazer isso.. quando entrar a view..
         Se algum de vocês dois puder fazer a ConsultaBO eu vou ficar bem agradecidor..
             */


        [TestMethod]
        public void TesteInserirPaciente()
        {
            using (PacienteFacade facade = new PacienteFacade())
            {
                facade.InserirPaciente("Teste", "123456789", DateTime.Today, "teste@teste.com","senha123");
               
            }
        }

        [TestMethod]
        public void TestarDoutor()
        {
            using (DoutorFacade facade = new DoutorFacade())
            {
                facade.InserirDoutor("Nadia","06693602959",DateTime.Today, 1010);

                var lista = facade.ListarDoutores();
                Assert.IsNull(lista);
            }
        }
    }
}
