﻿using System.Web;
using System.Web.Optimization;

namespace WebNew
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                    "~/Content/css/ubuntu.css",
                    "~/Content/css/titillum_web.css",
                    "~/Content/css/bootstrap.min.css",
                    "~/Content/css/font-awesome.min.css",
                    "~/Content/css/jQUery.lightninBox.css",
                    "~/Content/css/main.css",
                    "~/Content/css/pogo-slider.min.css",
                    "~/Content/css/pricetable.css",
                    "~/Content/css/style.css",
                    "~/Content/css/typo.css",
                    "~/Content/css/sweetalert2.min.css",
                    "~/Content/css/jquery.dataTables.min.css"
                ));

            bundles.Add(new ScriptBundle("~/Content/js")
                .Include("~/Content/js/jquery.min.js",
                "~/Content/js/jquery.pogo-slider.min.js",
                "~/Content/js/sweetalert2.min.js"
                , "~/Content/js/jquery.dataTables.min.js")
                .IncludeDirectory("~/Content", "*.js", true));

        }
    }
}
