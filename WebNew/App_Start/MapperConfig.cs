﻿using AutoMapper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebNew.Models;

namespace WebNew.App_Start
{
    public static class MapperConfig
    {
        public static void InicializarMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Consulta, ConsultaViewModel>()
                    .ForMember(dest => dest.Doutor, opt => opt.MapFrom(source => source.Doutor))
                    .ForMember(dest => dest.Paciente, opt => opt.MapFrom(source => source.Paciente))
                    .ForMember(dest => dest.DataConsulta, opt => opt.MapFrom(source => source.DataConsulta))
                    .ForMember(dest => dest.Consultorio, opt => opt.MapFrom(source => source.Consultorio))
                    .ForMember(dest => dest.TipoConsultaEnum, opt => opt.MapFrom(source => source.TipoConsultaEnum))
                    .ForMember(dest => dest.StatusConsultaEnum, opt => opt.MapFrom(source => source.StatusConsultaEnum));
            });
        }
    }
}