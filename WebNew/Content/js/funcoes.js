﻿function abrirModalLogin() {
    swal({
        html:
          '<form class="form-horizontal">' +
                '<fieldset>' +
                '<legend>Login</legend>' +
                '<div class="form-group">' +
                '  <label class="col-md-2 control-label" for="login">Email</label>  ' +
                '  <div class="col-md-10">' +
                '  <input id="login" name="login" type="text" placeholder="Login" class="form-control input-md" required="">' +
                '  </div>' +
                '</div>' +
                '<div class="form-group">' +
                '  <label class="col-md-2 control-label" for="senha">Senha</label>' +
                '  <div class="col-md-10">' +
                '    <input id="senha" name="senha" type="password" placeholder="Senha" class="form-control input-md" required="">' +
                '  </div>' +
                '</div>' +
                '<a class="pull-right" href="/Cadastro">Cadastre-se</a>'+
                '<div class="form-group">' +
                '   <div class="col-md-12">' +
                '       <button id="button1id" name="button1id" class="btn btn-primary">Logar</button>' +
                '       <button id="button2id" name="button2id" class="btn btn-danger">Cancelar</button>' +
                '   </div>' +
                '</div>' +
                '</fieldset>' +
          '</form>',
        showCloseButton: false,
        showCancelButton: false,
        showConfirmButton: false,
        confirmButtonText:
          '<i class="fa fa-thumbs-up"></i> Login',
        cancelButtonText:
          '<i class="fa fa-thumbs-down"></i> Cancelar'
    }).then(function () { console.log("oi") }, function (event) { console.log("evento" + event); });
}