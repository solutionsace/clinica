﻿using BusinessLayer.BO;
using BusinessLayer.Facade;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebNew.Controllers
{
    public class CadastroController : Controller
    {
        // GET: Cadastro
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Paciente paciente)
        {

            using (PacienteFacade facade = new PacienteFacade())
            {
                facade.InserirPaciente(paciente.Nome, paciente.CPF, paciente.DataNascimento,paciente.Email, paciente.Senha);
            }

            return View("Sucesso");

        }
    }
}