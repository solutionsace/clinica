﻿using BusinessLayer.BO;
using BusinessLayer.Facade;
using Model;
using Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebNew.Models;

namespace WebNew.Controllers
{
    public class ConsultasController : Controller
    {
        // GET: Consultas
        public ActionResult Index()
        {

            List<Consulta> consultas;
            using (ConsultaFacade consultaFacade = new ConsultaFacade())
            {
                consultas = consultaFacade.ListarConsultas();
            }

            return View(consultas);
        }


        public ActionResult Cadastrar()
        {
            var tipos = Enum.GetValues(typeof(TipoConsultaEnum));

            TempData["tipos"] = tipos;
            TempData["doutores"] = ListarDoutores();
            TempData["pacientes"] = ListarPacientes();
            TempData["consultorios"] = ListarConsultorios();

            return View();
        }

        [HttpPost]
        public ActionResult Cadastrar(Consulta consulta, int paciente, int doutor, int consultorio, string tipo)
        {
            using (PacienteFacade facade = new PacienteFacade())
            {
                consulta.Paciente = facade.BuscarPorId(paciente);
            }
            using (DoutorFacade facade = new DoutorFacade())
            {
                consulta.Doutor = facade.BuscarPorId(doutor);
            }
            using (ConsultorioFacade facade = new ConsultorioFacade())
            {
                consulta.Consultorio = facade.BuscarPorId(consultorio);
            }
            consulta.TipoConsultaEnum = TipoConsultaEnumMethods.GetTipo(tipo);
            try
            {
                using (ConsultaFacade facade = new ConsultaFacade())
                {
                    consulta.StatusConsultaEnum = StatusConsultaEnum.CONFIRMADA;

                    facade.InserirConsulta(consulta);
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return RedirectToAction("Index");
        }


        public ActionResult Editar(int id)
        {

            Consulta consulta;
            using (ConsultaFacade facade = new ConsultaFacade())
            {
                consulta = facade.BuscarPorId(id);
            }

            ConsultaViewModel consultaViewModel = new ConsultaViewModel();

            consultaViewModel.Paciente = consulta.Paciente;
            consultaViewModel.Doutor = consulta.Doutor;
            consultaViewModel.Consultorio = consulta.Consultorio;
            consultaViewModel.DataConsulta = consulta.DataConsulta;
            //consultaViewModel.Pagamento = consulta.Pagamento;
            consultaViewModel.StatusConsultaEnum = consulta.StatusConsultaEnum;
            consultaViewModel.TipoConsultaEnum = consulta.TipoConsultaEnum;

            var tipos = Enum.GetValues(typeof(TipoConsultaEnum));

            TempData["tipos"] = tipos;
            TempData["doutores"] = ListarDoutores();
            TempData["consultorios"] = ListarConsultorios();

            return View(consultaViewModel);
        }

        [HttpPost]
        public ActionResult Editar(Consulta consulta, int doutor, int consultorio, string tipo)
        {
            using (ConsultaFacade facadeC = new ConsultaFacade())
            {
                consulta = facadeC.BuscarPorId(consulta.Id);
                try
                {
                    using (DoutorFacade facade = new DoutorFacade())
                    {
                        consulta.Doutor = facade.BuscarPorId(doutor);
                    }
                    using (ConsultorioFacade facade = new ConsultorioFacade())
                    {
                        consulta.Consultorio = facade.BuscarPorId(consultorio);
                    }
                    consulta.TipoConsultaEnum = TipoConsultaEnumMethods.GetTipo(tipo);


                    consulta.StatusConsultaEnum = StatusConsultaEnum.CONFIRMADA;

                    facadeC.InserirConsulta(consulta);

                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return RedirectToAction("Index");
        }

        private List<Doutor> ListarDoutores()
        {
            List<Doutor> lista = null;
            using (DoutorFacade facade = new DoutorFacade())
            {
                lista = facade.ListarDoutores();
            }
            return lista;
        }

        private List<Paciente> ListarPacientes(int id = 0)
        {
            List<Paciente> lista = null;
            using (PacienteFacade facade = new PacienteFacade())
            {
                lista = facade.ListarPacientes(id);
            }
            return lista;
        }

        private List<Consultorio> ListarConsultorios()
        {
            List<Consultorio> lista = null;

            using (ConsultorioFacade facade = new ConsultorioFacade())
            {
                lista = facade.ListarConsultorios();
            }

            return lista;
        }
    }
}