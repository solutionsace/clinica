﻿using Model;
using Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebNew.Models
{
    public class ConsultaViewModel
    {
        public TipoConsultaEnum TipoConsultaEnum { get; set; }
        public StatusConsultaEnum StatusConsultaEnum { get; set; }
        public Doutor Doutor { get; set; }
        public Paciente Paciente { get; set; }
        public Consultorio Consultorio { get; set; }
        public Pagamento Pagamento { get; set; }
        public DateTime DataConsulta { get; set; }
    }
}